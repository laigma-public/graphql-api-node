import express from 'express'
import { graphqlHTTP } from 'express-graphql'
import { buildSchema } from  'graphql'
import morgan from 'morgan'
import cors from 'cors'
import customEnv from 'custom-env'

import dbService from '../services/dbService/dbService'

customEnv.env(process.env.NODE_ENV);

// Definimos esquema GraphQL
const schema = buildSchema(`
  type FilmData {
    film_id: Int
    title: String
    description: String
    release_year: Int
    rating: String
  }

  type Container {
    contenedor: String
    descripcion: String
  }

  type BufferOrder {
    orden: String
    estado: String
  }

  type Message {
    msg: String
  }

  type Query {
    getFilm(name: String!): [FilmData]
    getFilmsByActor(actor: String!): [FilmData]
    getContainers: [Container]
    getBufferOrdersByStatus(status: String): [BufferOrder]
  }

  type Mutation {
    changeFilmDescription(name: String!, description: String!): [FilmData],    
    removeFilm(name: String!): Message
  }
`);



const root = {
  getFilm: async ({name}) => {
    const res = await dbService.execQuery(`SELECT film_id, title, description, release_year, rating FROM film WHERE title = '${name}'`);
    console.log(res);
    return res;
  },

  getFilmsByActor: async ({actor}) => {    
    const res = await dbService.execQuery(
      `SELECT * FROM film f 
        JOIN film_actor fa ON f.film_id = fa.film_id 
        JOIN actor a ON a.actor_id = fa.actor_id WHERE a.first_name = '${actor}'`);
    console.log(res);
    return res;
  },

  changeFilmDescription: async ({name, description}) => {
    const res = await dbService.execQuery(
      `UPDATE film SET description = '${description}' 
      WHERE title = '${name}' RETURNING *`);
    console.log(res);
    return res;
  },

  removeFilm: async ({name}) => {
    // Problemas con FK de la db de prueba
    return ({ msg: name });
  },

  getContainers: async () => {
    const res = await dbService.execQuery('SELECT * FROM contenedores');
    return res;
  },

  getBufferOrdersByStatus: async ({status}) => {
    console.log(status);
    const query = status ? `SELECT * FROM buffer_ordenes WHERE estado = '${status}'` : `SELECT * FROM buffer_ordenes`;
    const res = await dbService.execQuery(query);
    return res;
  }
};

const app = express();

app.use(morgan('dev'));
app.use(cors());

app.use('/graphql', graphqlHTTP({
  schema: schema,
  rootValue: root,
  graphiql: true,
}));
app.listen(process.env.API_PORT, () => {
  console.log("Ejecutando servidor de GraphQL:");
  console.log('\x1b[33m%s\x1b[0m', `http://${process.env.API_HOST}:${process.env.API_PORT}/graphql`);
});