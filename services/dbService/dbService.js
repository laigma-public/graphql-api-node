import pg from 'pg';
import customEnv from 'custom-env'

customEnv.env(process.env.NODE_ENV);

const pool = new pg.Pool({
  user: process.env.DB_USER,
  host: process.env.DB_HOST,
  database: process.env.DB_NAME,
  password: process.env.DB_PASSWORD,
  port: process.env.DB_PORT,
});

export default {
  async execQuery (query) {
    return new Promise((resolve) => {
      pool.query(query, (error, results) => {
        if (error) {
          throw error
        }
        resolve(results.rows) 
      })
    })
  }
}