# Plantilla GraphQL con PostgreSQL en ES6
- Configurar credenciales en archivos .env.development , .env.integration y .env.production:
  DB_USER, DB_HOST, DB_NAME, DB_PASSWORD, DB_PORT, API_HOST, API_PORT
- Ejecutar npm run start/int/dev para diferentes entornos.

# Indice
- 01. Hola Mundo! -> git checkout bd1e02cdcb3762bf15254d36793cb8d814e80a25
- 02. Tipos Basicos -> git checkout 3f9278b2fa1fc583bfafc96a3c95d54df5598f07
- 03. Pasando parámetros -> git checkout 4116c87da80c9743b335966f8a7a1afd6acc84d8
- 04. Tipo objeto -> git checkout a0eca834b97e807f3ada57789653c8ae3ecf8b15
- 05. Mutaciones y tipos Input -> git checkout 58bb0f1bd25fe3b306adb173b08499de18f28a47