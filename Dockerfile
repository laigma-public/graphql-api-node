# build stage
FROM node:14.18.2
WORKDIR /src
COPY . ${WORKDIR}
RUN npm install
EXPOSE 4000
CMD [ "npm", "run", "-u", "start" ]
